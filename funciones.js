function init(){
    var mymap = L.map('mapid').setView([4.6411929,-74.1643668], 16);
    //4.6411929,-74.1643668,16

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoiZGllZ29sb3phbm8yMiIsImEiOiJja2N3OWdkOXYwNm8yMndsNWlianluZDJkIn0.4esWDar_Dul5PsTGa2gMGw'
    }).addTo(mymap);

    var marker1 = L.marker([4.6547626,-74.0834089],  17).addTo(mymap); // Simon Bolivar
    var marker2 = L.marker([4.6635743,-74.0893057],  17).addTo(mymap); // Parque Salitre
    var marker3 = L.marker([4.5976153,-74.0835651],  17).addTo(mymap); // Parque tercer milenio
    var marker4 = L.marker([4.6436097,-74.1721367,], 20).addTo(mymap); // Panaderia uno A

   // Esto es para darle texto a cada marcador 
    marker1.bindPopup("<b>Mucho Gusto!</b><br>Soy el parque Simon Bolivar.").openPopup();
    marker2.bindPopup("<b>Hola Bienvenido !</b><br>Soy El Parque el Salitre.").openPopup();
    marker3.bindPopup("<b>Hola Bienvenido !</b><br>Soy el parque tercer Milenio.").openPopup();
    marker4.bindPopup("<b>Mucho Gusto!</b><br>Soy la mejor Panaderia de Patio Bonito.").openPopup();

    
}

